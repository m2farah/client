package m2.tiir.ifi.client.controller;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class RootCtrl {
	
//	@RequestMapping("/")
//	public ModelAndView redirect_client_path(ModelMap model) {
//        return new ModelAndView("redirect:/client", model);
//    }mioni
	
	@RequestMapping("/")
	public ModelAndView showIndexPage(ModelMap model) {
		return new ModelAndView("index");
	}
} 
