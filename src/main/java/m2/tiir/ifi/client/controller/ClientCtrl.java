package m2.tiir.ifi.client.controller;

import java.sql.Timestamp;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import m2.tiir.ifi.client.dao.Client;
import m2.tiir.ifi.client.service.ClientService;

@RestController
@RequestMapping("/client")
public class ClientCtrl {
	
	@Autowired
	private ClientService clientService;

	@RequestMapping("/")
	public String reqDispatcher() {
	    return "ici on dispatch les requestes";
	}
	
	@GetMapping("/login")
	public ModelAndView getLoginForm(ModelMap model) {
		return new ModelAndView("login").addObject("client", new Client());
	}
	
	@PostMapping("/login")
	public void postLoginForm(Client client) {
		
	}
	
	@GetMapping("/registration")
	public ModelAndView getRegistrationForm(ModelMap model) {
		return new ModelAndView("registration").addObject("client", new Client());
	}
	
	@PostMapping(value="/registration")//, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView postRegistrationForm(@Valid Client client, ModelMap model) {
		client.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		if (clientService.addClient(client))
			return new ModelAndView("redirect:list");
		else {
			ModelAndView mv = new ModelAndView("registration");
			mv.addObject("error", new String("Erreur: imposible d'ajouter cet utilisateur"));
			mv.addObject("client", client);
			return mv;
		}
	}
	
	
	@RequestMapping("/list")
	public ModelAndView getClients(ModelMap model){
		return new ModelAndView("allClients").addObject("clients", clientService.getAllClients());
	}
}
