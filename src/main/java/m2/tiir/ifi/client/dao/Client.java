package m2.tiir.ifi.client.dao;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.sql.Timestamp;


/**
 * The persistent class for the client database table.
 * 
 */
@Entity
//@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/**** Authentication fields*/
	@NotNull
	private String email;
	@NotNull
	private String password;
	/*****/
	
	@NotNull
	private String firstname;
	@NotNull
	private String lastname;
	private Timestamp created_at;
	private String address;
	private String city;
	private String pc;

	
	public Client() {
		
	}
	
	/**
	 * Just to initialize the admin account
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param password
	 */
//	public Client(String firstname, String lastname, String email, String password) {
//		this.firstname=firstname;
//		this.lastname=lastname;
//		this.email=email;
//		this.password=password; // before to launch the application in to production. We have to hash the password !
//	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Timestamp getCreatedAt() {
		return this.created_at;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.created_at = createdAt;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPc() {
		return this.pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

}