package m2.tiir.ifi.client.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import m2.tiir.ifi.client.dao.Client;

public interface InfClientRepository extends CrudRepository<Client, Long>{
	
	
	Client findClientByEmail(String email);
	List<Client> findAll();
}
