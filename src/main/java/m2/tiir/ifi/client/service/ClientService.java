package m2.tiir.ifi.client.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import m2.tiir.ifi.client.dao.Client;
import m2.tiir.ifi.client.repository.InfClientRepository;

@Service
public class ClientService {
	
//	private Client admin = new Client("admin","admin","admin@admin.com","admin");
	
	@Autowired
	private InfClientRepository clientRepository; // Spring do implemant the interface for us
	
	/**
	 * Just initialise the databes with a user admin (passwd=admin)
	 */
	public ClientService(){
	}
	
	
	/**
	 * This method return a list of client in the database.
	 * @return List of client. 
	 */
	public List<Client> getAllClients(){
		return clientRepository.findAll();
	}
	
	public Client getClientByEmail(String email) {
		return clientRepository.findClientByEmail(email);
	}
	
	public boolean addClient(Client client) {
		if (clientRepository.save(client).equals(null))
			return false;
		else
			return true;
	}
}
